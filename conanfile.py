#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil
from conans import (
    CMake,
    ConanFile,
    tools,
)


class DxflibConan(ConanFile):
    name = "dxflib"
    version = "3.17.0"
    homepage = "http://www.qcad.org/en/what-is-dxflib"
    url = "https://gitlab.com/grisumbras/conan-dxflib"
    description = "dxflib is a C++ library for reading and writing DXF files"
    license = "GPL2"
    settings = "os", "compiler", "build_type", "arch",
    exports_sources = "dxflib.pc.in",
    generators = "cmake",

    def source(self):
        url = (
            "http://qcad.org/archives/dxflib/dxflib-%s-src.tar.gz"
            % self.version
        )
        tools.get(url, filename="dxflib.tar.gz")

        if os.path.exists("src"):
            shutil.rmtree("src")
        shutil.move("dxflib-%s-src" % self.version, "src")
        shutil.move("dxflib.pc.in", "src")

        with open(os.path.join("src", "CMakeLists.txt"), 'w') as f:
            f.write(_cml_contents.format(
                version=self.version,
                description=self.description,
                url=self.homepage,
            ))

    def build(self):
        cmake = self._builder()
        cmake.build()

    def package(self):
        cmake = self._builder()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ['dxflib']
        self.env_info.PKG_CONFIG_PATH.append(
            os.path.join(self.package_folder, 'lib', 'pkgconfig')
        )

    def _builder(self):
        cmake = CMake(self)
        cmake.configure(
            build_folder=os.path.join(self.build_folder, "build"),
            source_folder=os.path.join(self.source_folder, "src"),
            defs={'DXFLIB_CONAN_INSTALL_DIR': self.install_folder},
        )
        return cmake


_cml_contents = '''
cmake_minimum_required(VERSION 3.8)

project(dxflib
    VERSION "{version}"
    DESCRIPTION "{description}"
    LANGUAGES CXX
)
include("${{DXFLIB_CONAN_INSTALL_DIR}}/conanbuildinfo.cmake")
conan_basic_setup()
include(GNUInstallDirs)
set(PROJECT_HOMEPAGE_URL "{url}")

file(GLOB lib_sources src/*.cpp)
add_library(dxflib ${{lib_sources}})
target_compile_definitions(dxflib PRIVATE DXFLIB_LIBRARY)

install(TARGETS dxflib DESTINATION "${{CMAKE_INSTALL_LIBDIR}}")

install(
    DIRECTORY src/
    DESTINATION "${{CMAKE_INSTALL_INCLUDEDIR}}/dxflib"
    FILES_MATCHING PATTERN "*.h"
)

configure_file(dxflib.pc.in "${{CMAKE_CURRENT_BINARY_DIR}}/dxflib.pc" @ONLY)
install(
    FILES "${{CMAKE_CURRENT_BINARY_DIR}}/dxflib.pc"
    DESTINATION "${{CMAKE_INSTALL_LIBDIR}}/pkgconfig"
)
install(
    FILES gpl-2.0greater.txt
    DESTINATION "${{CMAKE_INSTALL_DATADIR}}/dxflib"
    RENAME LICENSE
)
'''
